'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.createTable('user_invites', {
          user_invite_id: {
              type: Sequelize.BIGINT(20),
              autoIncrement: true,
              primaryKey: true,
              allowNull: false
          },
          user_uuid: {
              type: Sequelize.STRING(50)
          },
          email: {
              type: Sequelize.STRING(255)
          },
          accepted: {
              type: Sequelize.BOOLEAN,
              allowNull: false,
              defaultValue: 0
          },
          created_at: {
              type: Sequelize.DATE
          },
          updated_at: {
              type: Sequelize.DATE
          },
          deleted_at: {
              type: Sequelize.DATE
          }
      });
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.dropTable('user_invites');
  }
};
