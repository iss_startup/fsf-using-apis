'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.createTable('user_password_resets', {
          user_reset_id: {
              type: Sequelize.BIGINT(20),
              autoIncrement: true,
              primaryKey: true,
              allowNull: false
          },
          user_uuid: {
              type: Sequelize.STRING(50)
          },
          reset_token: {
              type: Sequelize.STRING(100)
          },
          created_at: {
              type: Sequelize.DATE
          },
          updated_at: {
              type: Sequelize.DATE
          },
          deleted_at: {
              type: Sequelize.DATE
          }
      });
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.dropTable('user_password_resets');
  }
};
