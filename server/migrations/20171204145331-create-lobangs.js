'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('lobangs', {
            lobang_id: {
                type: Sequelize.BIGINT(20),
                autoIncrement: true,
                primaryKey: true,
                allowNull: false
            },
            lobang_uuid: {
                type: Sequelize.STRING(50)
            },
            user_uuid: {
                type: Sequelize.STRING(50)
            },
            lobang_name: {
                type: Sequelize.STRING(255)
            },
            lobang_start: {
                type: Sequelize.DATE
            },
            lobang_end: {
                type: Sequelize.DATE
            },
            lobang_imgs: {
                type: Sequelize.TEXT
            },
            lobang_description: {
                type: Sequelize.TEXT
            },
            lobang_status: {
                type: Sequelize.INTEGER(2),
                allowNull: false,
                defaultValue: 0
            },
            created_at: {
                type: Sequelize.DATE
            },
            updated_at: {
                type: Sequelize.DATE
            },
            deleted_at: {
                type: Sequelize.DATE
            }
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('lobangs');
    }
};
