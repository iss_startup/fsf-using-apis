'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.createTable('users', {
          user_id: {
              type: Sequelize.BIGINT(20),
              autoIncrement: true,
              primaryKey: true,
              allowNull: false
          },
          user_uuid: {
              type: Sequelize.STRING(50),
              primaryKey: true
          },
          email: {
              type: Sequelize.STRING(255)
          },
          mobile: {
              type: Sequelize.STRING(50)
          },
          password: {
              type: Sequelize.STRING(100)
          },
          remember_token: {
              type: Sequelize.STRING(100)
          },
          consent_token: {
              type: Sequelize.STRING(100)
          },
          role_id: {
              type: Sequelize.BIGINT(20)
          },
          payment_id: {
              type: Sequelize.BIGINT(20)
          },
          created_at: {
              type: Sequelize.DATE
          },
          updated_at: {
              type: Sequelize.DATE
          },
          deleted_at: {
              type: Sequelize.DATE
          }
      });
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.dropTable('users');
  }
};
