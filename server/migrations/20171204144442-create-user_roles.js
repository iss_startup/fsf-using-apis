'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.createTable('user_roles', {
          user_role_id: {
              type: Sequelize.BIGINT(20),
              autoIncrement: true,
              primaryKey: true,
              allowNull: false
          },
          role: {
              type: Sequelize.STRING(20),
              allowNull: false,
              defaultValue: 'Choper'
          },
          permissions: {
              type: Sequelize.TEXT
          },
          created_at: {
              type: Sequelize.DATE
          },
          updated_at: {
              type: Sequelize.DATE
          },
          deleted_at: {
              type: Sequelize.DATE
          }
      });
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.dropTable('user_roles');
  }
};
