'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.addColumn('barangs', 'barang_category', {
          type: Sequelize.STRING(255),
          allowNull: true,
          after: 'barang_name'
      });
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.removeColumn('barangs', 'barang_category');
  }
};
