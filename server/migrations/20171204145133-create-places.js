'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.createTable('places', {
          place_id: {
              type: Sequelize.BIGINT(20),
              autoIncrement: true,
              primaryKey: true,
              allowNull: false
          },
          place_uuid: {
              type: Sequelize.STRING(50)
          },
          user_uuid: {
              type: Sequelize.STRING(50)
          },
          place_name: {
              type: Sequelize.STRING(255)
          },
          place_address: {
              type: Sequelize.STRING(255)
          },
          waypoint: {
              type: Sequelize.TEXT
          },
          place_imgs: {
              type: Sequelize.TEXT
          },
          place_description: {
              type: Sequelize.TEXT
          },
          place_status: {
              type: Sequelize.INTEGER(2),
              allowNull: false,
              defaultValue: 0
          },
          created_at: {
              type: Sequelize.DATE
          },
          updated_at: {
              type: Sequelize.DATE
          },
          deleted_at: {
              type: Sequelize.DATE
          }
      });
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.dropTable('places');
  }
};
