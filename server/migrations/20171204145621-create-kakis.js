'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('kakis', {
            kaki_id: {
                type: Sequelize.BIGINT(20),
                autoIncrement: true,
                primaryKey: true,
                allowNull: false
            },
            kaki_uuid: {
                type: Sequelize.STRING(50)
            },
            user_uuid: {
                type: Sequelize.STRING(50)
            },
            kaki_type: {
                type: Sequelize.STRING(255)
            },
            kaki_icon: {
                type: Sequelize.STRING(20)
            },
            created_at: {
                type: Sequelize.DATE
            },
            updated_at: {
                type: Sequelize.DATE
            },
            deleted_at: {
                type: Sequelize.DATE
            }
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('kakis');
    }
};
