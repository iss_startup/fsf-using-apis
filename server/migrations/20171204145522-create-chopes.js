'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('chopes', {
            chope_id: {
                type: Sequelize.BIGINT(20),
                autoIncrement: true,
                primaryKey: true,
                allowNull: false
            },
            that_uuid: {
                type: Sequelize.STRING(50)
            },
            user_uuid: {
                type: Sequelize.STRING(50)
            },
            that_type: {
                type: Sequelize.STRING(20),
                allowNull: false,
                defaultValue: 'Place'
            },
            created_at: {
                type: Sequelize.DATE
            },
            updated_at: {
                type: Sequelize.DATE
            },
            deleted_at: {
                type: Sequelize.DATE
            }
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('chopes');
    }
};
