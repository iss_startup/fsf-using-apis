'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('barangs', {
            barang_id: {
                type: Sequelize.BIGINT(20),
                autoIncrement: true,
                primaryKey: true,
                allowNull: false
            },
            barang_uuid: {
                type: Sequelize.STRING(50)
            },
            user_uuid: {
                type: Sequelize.STRING(50)
            },
            barang_name: {
                type: Sequelize.STRING(255)
            },
            barang_imgs: {
                type: Sequelize.TEXT
            },
            barang_description: {
                type: Sequelize.TEXT
            },
            barang_status: {
                type: Sequelize.INTEGER(2),
                allowNull: false,
                defaultValue: 0
            },
            created_at: {
                type: Sequelize.DATE
            },
            updated_at: {
                type: Sequelize.DATE
            },
            deleted_at: {
                type: Sequelize.DATE
            }
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('barangs');
    }
};
