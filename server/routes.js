'use strict';

const express = require('express');
const path = require('path');
const UsersController = require('./controllers/users.controller');
const ChopekingController = require('./controllers/chopeking.controller');
const GotController = require('./controllers/got.controller');

const CLIENT_FOLDER = path.join(__dirname + '/../public');

module.exports = {
    init: configureRoutes,
    errorHandler: errorHandler
};

function configureRoutes(app){

    app.get('/users', UsersController.list);
    app.post('/users', UsersController.create);
    app.get('/users/:id', UsersController.show);
    app.put('/users/:id', UsersController.update);
    app.delete('/users/:id', UsersController.delete);

    app.get('/chopes/:id', ChopekingController.chopeLe);
    app.post('/chopes/:id', ChopekingController.chopeAh);
    app.delete('/chopes/:id', ChopekingController.unChope);
    app.get('/choped/places/:id', ChopekingController.chopedBy);
    app.get('/kakis/:id', ChopekingController.myKakis);
    app.post('/kakis/:id', ChopekingController.beKaki);
    app.delete('/kakis/:id', ChopekingController.unKaki);

    app.get('/got/:meh', GotController.list);
    app.post('/got/:meh', GotController.create);
    app.get('/got/:meh/:id', GotController.show);
    app.put('/got/:meh/:id', GotController.update);
    app.delete('/got/:meh/:id', GotController.delete);

    app.use(express.static(CLIENT_FOLDER));

}

function errorHandler(app) {
    app.use(function (req, res) {
        res.status(400).sendFile(CLIENT_FOLDER + '/404.html');
    });

    app.use(function (err, req, res, next) {
        console.log(err);
        res.status(500).sendFile(path.join(CLIENT_FOLDER + '/500.html'));
    });
};

