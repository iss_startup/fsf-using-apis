const Sequelize = require('sequelize');
const Barangs = require('../models/').barangs;
const Lobangs = require('../models/').lobangs;
const Places = require('../models/').places;
const MapsController = require('./maps.controller');

const config = require('../config'),
      https = require('https'),
      xmlparse = require('xml2json'),
      moment = require('moment'),
      uuid = require('uuid/v4'),
      validate = require('uuid-validate');

const mehType = {
    what: 'barangs',
    when: 'lobangs',
    where: 'places'
};

var getMeh = function(meh){
    if(meh=='where') {
        return Places;
    } else if(meh=='when') {
        return Lobangs;
    } else {
        return Barangs;
    }
};

var identifyMeh = function(meh,id){
    if(meh=='where') {
        return {
            place_uuid: id
        };
    } else if(meh=='when') {
        return {
            lobang_uuid: id
        };
    } else {
        return {
            barang_uuid: id
        };
    }
};

var constructMeh = function(meh,field,id){
    if(meh=='where') {
        return {
            place_uuid: id,
            user_uuid: field.user_uuid,
            place_name: field.place_name,
            place_address: field.place_address,
            place_description: field.place_description,
            waypoint: field.waypoint,
            place_imgs: []
        };
    } else if(meh=='when') {
        return {
            lobang_uuid: id,
            user_uuid: field.user_uuid,
            lobang_name: field.lobang_name,
            lobang_start: field.lobang_start,
            lobang_end: field.lobang_end,
            lobang_description: field.lobang_description,
            lobang_imgs: []
        };
    } else {
        return {
            barang_uuid: id,
            user_uuid: field.user_uuid,
            barang_name: field.barang_name,
            barang_category: field.barang_category,
            barang_description: field.barang_description,
            barang_imgs: field.barang_imgs
        };
    }
};

exports.list = function(req,res){
    var meh = req.params.meh;

    var whereCondition = {};
    var page = parseInt(req.query.page) || 1;
    var items = parseInt(req.query.items) || config.pagination.default;
    var offset = (page - 1) * items;
    var limit = items;
    var sortBy = req.query.sortBy || 'ASC';
    var order = mehType[meh]+'.created_at '+sortBy;

    whereCondition = {
        order: order,
        offset: offset,
        limit: limit
    };

    getMeh(meh)
        .findAndCountAll(whereCondition)
        .then(got => {
            if(got) {
                console.log(got);
                res.status(200).json(got);
            } else {
                res.status(401).send(JSON.stringify('Got nothing'));
            }
        });
};

exports.create = function(req,res){
    var meh = req.params.meh;

    if(meh=='where') {
        MapsController.findWaypoint(req.body, res).then(waypoint => {
            var geoLocation = waypoint;
            console.log(geoLocation[0].geometry.location);
            req.body.waypoint = geoLocation[0].geometry.location.lat+','+geoLocation[0].geometry.location.lng;

            getMeh(meh)
                .create(constructMeh(meh,req.body,uuid()))
                .then(got => {
                    res.status(200).json(got);
                }).catch(err => {
                var errorMsg;
                if(config.debug==true) {
                    errorMsg = err;
                } else {
                    errorMsg = 'Creation Failed';
                }
                res
                    .status(500)
                    .json({error: true, errorText: errorMsg});
            });
        }).catch(err => {
            var errorMsg;
            if(config.debug==true) {
                errorMsg = err;
            } else {
                errorMsg = 'Geocoding Failed';
            }
            res
                .status(500)
                .json({error: true, errorText: errorMsg});
        });
    } else if(meh=='what') {
        if(req.body.barang_category=='Book') {
            https.get(config.goodreads.url+'/search/?key='+config.goodreads.key+'&q='+encodeURIComponent(req.body.barang_name), results => {
                results.on('data', search => {
                    var grResults = JSON.parse(xmlparse.toJson(search));
                    var books = grResults.GoodreadsResponse.search.results.work;

                    console.log(books);
                    req.body.barang_imgs = [books[0].best_book.image_url];

                    getMeh(meh)
                        .create(constructMeh(meh,req.body,uuid()))
                        .then(got => {
                            res.status(200).json(got);
                        }).catch(err => {
                        var errorMsg;
                        if(config.debug==true) {
                            errorMsg = err;
                        } else {
                            errorMsg = 'Creation Failed';
                        }
                        res
                            .status(500)
                            .json({error: true, errorText: errorMsg});
                    });
                });

                results.on('error', err => {
                    res.status(results.statusCode).json(JSON.parse(err.toString()));
                });
            });
        } else {
            req.body.barang_imgs = [];

            getMeh(meh)
                .create(constructMeh(meh,req.body,uuid()))
                .then(got => {
                    res.status(200).json(got);
                }).catch(err => {
                var errorMsg;
                if(config.debug==true) {
                    errorMsg = err;
                } else {
                    errorMsg = 'Creation Failed';
                }
                res
                    .status(500)
                    .json({error: true, errorText: errorMsg});
            });
        }
    } else {
        getMeh(meh)
            .create(constructMeh(meh,req.body,uuid()))
            .then(got => {
                res.status(200).json(got);
            }).catch(err => {
            var errorMsg;
            if(config.debug==true) {
                errorMsg = err;
            } else {
                errorMsg = 'Creation Failed';
            }
            res
                .status(500)
                .json({error: true, errorText: errorMsg});
        });
    }
};

exports.show = function(req,res){
    var meh = req.params.meh;

    var id = req.params.id;
    if(validate(id)==true) {
        getMeh(meh)
            .findOne({
                where: identifyMeh(meh,id)
            })
            .then(got => {
                if(got) {
                    console.log(got);
                    res.status(200).json(got);
                } else {
                    res.status(401).send(JSON.stringify('Got nothing'));
                }
            });
    } else {
        res.status(500).send(JSON.stringify('ID buay sai'));
    }
};

exports.update = function(req,res){
    var meh = req.params.meh;

    var id = req.params.id;
    if(validate(id)==true) {
        getMeh(meh)
            .findOne({
                where: identifyMeh(meh,id)
            })
            .then(got => {
                if(got) {
                    var gotNew = {};
                    var gotFields = Object.keys(got.dataValues);
                    gotFields.forEach(field => {
                        if(typeof req.body[field] != 'undefined') {
                            if(req.body[field] != got[field]) {
                                var obj = {};
                                obj[field] = req.body[field];
                                gotNew = Object.assign(gotNew, obj);
                            }
                        }
                    });
                    console.log(gotNew);

                    if(meh=='where' && typeof gotNew.place_address!='undefined') {
                        MapsController.findWaypoint(req.body, res).then(waypoint => {
                            var geoLocation = waypoint;
                            console.log(geoLocation[0].geometry.location);
                            gotNew.waypoint = geoLocation[0].geometry.location.lat+','+geoLocation[0].geometry.location.lng;

                            got.updateAttributes(constructMeh(meh,gotNew,id)).then(gotten => {
                                res.status(200).json(gotten);
                            }).catch(function (err){
                                var errorMsg;
                                if(config.debug==true) {
                                    errorMsg = err;
                                } else {
                                    errorMsg = 'Update Failed';
                                }
                                res
                                    .status(500)
                                    .json({error: true, errorText: errorMsg});
                            });
                        }).catch(err => {
                            var errorMsg;
                            if(config.debug==true) {
                                errorMsg = err;
                            } else {
                                errorMsg = 'Geocoding Failed';
                            }
                            res
                                .status(500)
                                .json({error: true, errorText: errorMsg});
                        });
                    } else {
                        gotNew.waypoint = '0,0';

                        got.updateAttributes(constructMeh(meh,gotNew,id)).then(gotten => {
                            res.status(200).json(gotten);
                        }).catch(function (err){
                            var errorMsg;
                            if(config.debug==true) {
                                errorMsg = err;
                            } else {
                                errorMsg = 'Update Failed';
                            }
                            res
                                .status(500)
                                .json({error: true, errorText: errorMsg});
                        });
                    }
                } else {
                    res.status(401).send(JSON.stringify('Got nothing'));
                }
            });
    } else {
        res.status(500).send(JSON.stringify('ID buay sai'));
    }
};

exports.delete = function(req,res){
    var meh = req.params.meh;

    var id = req.params.id;
    if(validate(id)==true) {
        getMeh(meh)
            .destroy({
                where: identifyMeh(meh,id)
            })
            .then(got => {
                if(got) {
                    console.log('Got no more');
                    res.status(200).json(got);
                } else {
                    res.status(401).send(JSON.stringify('Cannot destroy'));
                }
            });
    } else {
        res.status(500).send(JSON.stringify('ID buay sai'));
    }
};
