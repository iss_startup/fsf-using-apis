const Users = require('../models/').users;
const UserProfiles = require('../models/').user_profiles;
const UserRoles = require('../models/').user_roles;
const UserPasswordResets = require('../models/').user_password_resets;

const config = require('../config'),
      moment = require('moment'),
      uuid = require('uuid/v4'),
      validate = require('uuid-validate'),
      bcrypt = require('bcrypt');
//const mailgun = require('mailgun-js')({apiKey: config.mailgun.key, domain: config.mailgun.domain});

// Without using any templating engine
//var email = require('../../email.template');

exports.list = function(req,res){
    var whereCondition = {};
    var page = parseInt(req.query.page) || 1;
    var items = parseInt(req.query.items) || config.pagination.default;
    var offset = (page - 1) * items;
    var limit = items;
    var sortBy = req.query.sortBy || 'DESC';
    var order = 'users.created_at '+sortBy;
    var keyword = req.query.keyword || '';

    whereCondition = {
        attributes: {
            exclude: ['user_id', 'password', 'remember_token', 'consent_token']
        },
        include: [
            {
                model: UserProfiles,
                attributes: [
                    'name_family', 'name_given'
                ],
                where: {
                    $or: [
                        {name_given: {like: '%' + keyword + '%'}},
                        {name_family: {like: '%' + keyword + '%'}}
                    ]
                }
            }
        ],
        order: order,
        offset: offset,
        limit: limit
    }

    Users
        .findAndCountAll(whereCondition)
        .then(function (results) {
            if (results) {
                console.log(results);
                res.status(200).json(results);
            } else {
                res.status(401).send(JSON.stringify('No Users Found'));
            }
        });
};

exports.create = function(req,res){
    bcrypt.hash(req.body.password, 10).then(function(hashedPass) {
        Users
            .findOrCreate({
                where: {
                    email: req.body.email
                },
                defaults: {
                    user_uuid: uuid(),
                    mobile: req.body.mobile,
                    password: hashedPass,
                    role_id: 1
                }
            })
            .spread((results, created) => {
                if (created==true) {
                    UserProfiles
                        .create({
                            user_id: results.user_id,
                            user_uuid: results.user_uuid,
                            name_family: req.body.name_family,
                            name_given: req.body.name_given,
                            salutation: req.body.salutation,
                            gender: req.body.gender,
                            birthdate: req.body.birthdate,
                            address: req.body.address,
                            state: req.body.state,
                            country: req.body.country
                        })
                        .then(user => {
                            console.log(results);
                            res.status(200).json(user);
                        }).catch(err => {
                            var errorMsg;
                            if(config.debug==true) {
                                errorMsg = err;
                            } else {
                                errorMsg = 'Creation Failed';
                            }
                            res
                                .status(500)
                                .json({error: true, errorText: errorMsg});
                    });
                } else {
                    res.status(401).send(JSON.stringify('User Already Registered'));
                }
            });
    });
};

exports.show = function(req,res){
    var userId = req.params.id;
    if(validate(userId)==true) {
        Users
            .findOne({
                attributes: {
                    exclude: ['user_id', 'password', 'remember_token', 'consent_token']
                },
                include: [
                    {
                        model: UserProfiles,
                        attributes: {
                            exclude: ['user_profile_id', 'user_id', 'user_uuid']
                        }
                    }
                ],
                where: {
                    user_uuid: userId
                }
            })
            .then(function(results){
                if (results) {
                    console.log(results);
                    res.json(results);
                } else {
                    res.status(401).send(JSON.stringify('User Not Found'));
                }
            });
    } else {
        res.status(500).send(JSON.stringify('Invalid User ID'));
    }
};

exports.update = function(req,res){
    var userId = req.params.id;
    if(validate(userId)==true) {
        UserProfiles
            .find({
                where: {
                    user_uuid: userId
                }
            })
            .then(function(results){
                results.updateAttributes({
                    name_family: req.body.name_family,
                    name_given: req.body.name_given,
                    salutation: req.body.salutation,
                    gender: req.body.gender,
                    birthdate: req.body.birthdate,
                    address: req.body.address,
                    state: req.body.state,
                    country: req.body.country
                }).then(function (){
                    res.status(200).end();
                }).catch(function (err){
                    var errorMsg;
                    if(config.debug==true) {
                        errorMsg = err;
                    } else {
                        errorMsg = 'Update Failed';
                    }
                    res
                        .status(500)
                        .json({error: true, errorText: errorMsg});
                });
            });
    } else {
        res.status(500).send(JSON.stringify('Invalid User ID'));
    }
};

exports.delete = function(req,res){
    var userId = req.params.id;
    if(validate(userId)==true) {
        UserProfiles
            .destroy({
                where: {
                    user_uuid: userId
                }
            })
            .then(function(){
                console.log('User Profile Deleted');
                Users
                    .destroy({
                        where: {
                            user_uuid: userId
                        }
                    })
                    .then(function(results){
                        console.log('User Deleted');
                        res.status(200).json(results);
                    })
                    .catch(function(err){
                        var errorMsg;
                        if(config.debug==true) {
                            errorMsg = err;
                        } else {
                            errorMsg = 'User Delete Failed';
                        }
                        res
                            .status(500)
                            .json({error: true, errorText: errorMsg});
                    });
            })
            .catch(function(err){
                var errorMsg;
                if(config.debug==true) {
                    errorMsg = err;
                } else {
                    errorMsg = 'User Profile Delete Failed';
                }
                res
                    .status(500)
                    .json({error: true, errorText: errorMsg});
        });
    } else {
        res.status(500).send(JSON.stringify('Invalid User ID'));
    }
};
