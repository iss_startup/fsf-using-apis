const Sequelize = require('sequelize');
const Chopes = require('../models/').chopes;
const Kakis = require('../models/').kakis;
const Users = require('../models/').users;
const UserProfiles = require('../models/').user_profiles;
const Places = require('../models/').places;
const MapsController = require('./maps.controller');

const config = require('../config'),
      moment = require('moment'),
      uuid = require('uuid/v4'),
      validate = require('uuid-validate'),
      DarkSky = require('dark-sky'),
      darksky = new DarkSky(config.darksky.key);

exports.chopeLe = function(req,res){
    var sortBy = req.query.sortBy || 'DESC';
    var order = 'chopes.created_at '+sortBy;
    var userIds = [];

    var thatId = req.params.id;
    if(validate(thatId)==true) {
        whereCondition = {
            attributes: ['user_uuid'],
            where: { that_uuid: thatId },
            order: order
        };

        Chopes
            .findAll(whereCondition)
            .then(chopes => {
                if(chopes) {
                    chopes.forEach(user => {
                        userIds.push(user.dataValues.user_uuid);
                    });
                    console.log(userIds);

                    order = 'users.created_at '+sortBy;
                    whereCondition = {
                        attributes: {
                            exclude: ['user_id', 'password', 'remember_token', 'consent_token']
                        },
                        include: [
                            {
                                model: UserProfiles,
                                attributes: {
                                    exclude: ['user_profile_id', 'user_id', 'user_uuid']
                                }
                            }
                        ],
                        where: { user_uuid: userIds },
                        order: order
                    };

                    Users
                        .findAndCountAll(whereCondition)
                        .then(users => {
                            if(users) {
                                console.log(users);
                                res.status(200).json(users);
                            } else {
                                res.status(401).send(JSON.stringify('Nobody chope lor!'));
                            }
                        });
                }
            });
    } else {
        res.status(500).send(JSON.stringify('ID buay sai leh'));
    }
};

exports.chopeAh = function(req,res){
    var thatId = req.params.id;
    if(validate(thatId)==true) {
        var userId = req.body.user_uuid;
        if(validate(userId)==true) {
            Chopes
                .create({
                    that_uuid: thatId,
                    user_uuid: userId,
                    that_type: req.body.type
                })
                .then(chope => {
                    res.status(200).json(chope);
                }).catch(err => {
                var errorMsg;
                if(config.debug==true) {
                    errorMsg = err;
                } else {
                    errorMsg = 'Cannot chope lah';
                }
                res
                    .status(500)
                    .json({error: true, errorText: errorMsg});
            });
        } else {
            res.status(500).send(JSON.stringify('ID buay sai leh'));
        }
    } else {
        res.status(500).send(JSON.stringify('ID buay sai leh'));
    }
};

exports.unChope = function(req,res){
    var thatId = req.params.id;
    if(validate(thatId)==true) {
        var userId = req.query.user_uuid;
        if(validate(userId)==true) {
            Chopes
                .destroy({
                    where: {
                        that_uuid: thatId,
                        user_uuid: userId
                    }
                })
                .then(chope => {
                    res.status(200).json(chope);
                })
                .catch(function(err){
                    var errorMsg;
                    if(config.debug==true) {
                        errorMsg = err;
                    } else {
                        errorMsg = 'Cannot unchope lah';
                    }
                    res
                        .status(500)
                        .json({error: true, errorText: errorMsg});
                });
        } else {
            res.status(500).send(JSON.stringify('ID buay sai leh'));
        }
    } else {
        res.status(500).send(JSON.stringify('ID buay sai leh'));
    }
};

exports.chopedBy = function(req,res){
    var sortBy = req.query.sortBy || 'DESC';
    var order = 'chopes.created_at '+sortBy;
    var thatIds = [];

    var userId = req.params.id;
    if(validate(userId)==true) {
        Chopes
            .findAll({
                attributes: ['that_uuid'],
                where: { user_uuid: userId },
                order: order
            })
            .then(chopes => {
                if(chopes) {
                    chopes.forEach(that => {
                        thatIds.push(that.dataValues.that_uuid);
                    });
                    console.log(thatIds);

                    order = 'places.created_at '+sortBy;
                    Places
                        .findAll({
                            attributes: ['place_name', 'waypoint'],
                            where: { place_uuid: thatIds },
                            order: order
                        })
                        .then(places => {
                            if(places) {
                                console.log(places);

                                if(req.query.show=='map') {
                                    MapsController.genStaticMap(places).then(map => {
                                        console.log(map);
                                        res.status(200).json(map);
                                    });
                                } else {
                                    if(req.query.show=='weather') {
                                        var markers = MapsController.latLng(places);
                                        var marker = markers[0];

                                        darksky
                                            .coordinates({lat: marker[0], lng: marker[1]})
                                            .units('si')
                                            .language('en')
                                            .exclude('minutely,hourly,daily,flags')
                                            .get()
                                            .then(weather => {
                                                console.log(weather);
                                                places.push(weather);
                                                res.status(200).json(places);
                                            })
                                            .catch(err => {
                                                var errorMsg;
                                                if(config.debug==true) {
                                                    errorMsg = err;
                                                } else {
                                                    errorMsg = 'Weather Forecast Failed';
                                                }
                                                res
                                                    .status(500)
                                                    .json({error: true, errorText: errorMsg});
                                            });
                                    } else {
                                        res.status(200).json(places);
                                    }
                                }
                            } else {
                                res.status(401).send(JSON.stringify('Cannot find any place leh'));
                            }
                        });
                }
            });
    } else {
        res.status(500).send(JSON.stringify('ID buay sai leh'));
    }
};

exports.myKakis = function(req,res){
    var sortBy = req.query.sortBy || 'DESC';
    var order = 'kakis.created_at '+sortBy;
    var userIds = [];

    var kakiId = req.params.id;
    if(validate(kakiId)==true) {
        whereCondition = {
            attributes: ['user_uuid'],
            where: { kaki_uuid: kakiId },
            order: order
        };

        Kakis
            .findAll(whereCondition)
            .then(kakis => {
                if(kakis) {
                    kakis.forEach(user => {
                        userIds.push(user.dataValues.user_uuid);
                    });
                    console.log(userIds);

                    order = 'users.created_at '+sortBy;
                    whereCondition = {
                        attributes: {
                            exclude: ['user_id', 'password', 'remember_token', 'consent_token']
                        },
                        include: [
                            {
                                model: UserProfiles,
                                attributes: {
                                    exclude: ['user_profile_id', 'user_id', 'user_uuid']
                                }
                            }
                        ],
                        where: { user_uuid: userIds },
                        order: order
                    };

                    Users
                        .findAndCountAll(whereCondition)
                        .then(users => {
                            if(users) {
                                console.log(users);
                                res.status(200).json(users);
                            } else {
                                res.status(401).send(JSON.stringify('You got no Kakis lor!'));
                            }
                        });
                }
            });
    } else {
        res.status(500).send(JSON.stringify('ID buay sai leh'));
    }
};

exports.beKaki = function(req,res){
    var kakiId = req.params.id;
    if(validate(kakiId)==true) {
        var userId = req.body.user_uuid;
        if(validate(userId)==true) {
            Kakis
                .create({
                    kaki_uuid: kakiId,
                    user_uuid: userId,
                    kaki_type: req.body.type,
                    kaki_icon: req.body.icon
                })
                .then(kaki => {
                    res.status(200).json(kaki);
                }).catch(err => {
                var errorMsg;
                if(config.debug==true) {
                    errorMsg = err;
                } else {
                    errorMsg = 'Kaki buay gam leh';
                }
                res
                    .status(500)
                    .json({error: true, errorText: errorMsg});
            });
        } else {
            res.status(500).send(JSON.stringify('ID buay sai leh'));
        }
    } else {
        res.status(500).send(JSON.stringify('ID buay sai leh'));
    }
};

exports.unKaki = function(req,res){
    var kakiId = req.params.id;
    if(validate(kakiId)==true) {
        var userId = req.query.user_uuid;
        if(validate(userId)==true) {
            Kakis
                .destroy({
                    where: {
                        kaki_uuid: kakiId,
                        user_uuid: userId
                    }
                })
                .then(kaki => {
                    res.status(200).json(kaki);
                })
                .catch(function(err){
                    var errorMsg;
                    if(config.debug==true) {
                        errorMsg = err;
                    } else {
                        errorMsg = 'Kaki mai pang seh';
                    }
                    res
                        .status(500)
                        .json({error: true, errorText: errorMsg});
                });
        } else {
            res.status(500).send(JSON.stringify('ID buay sai leh'));
        }
    } else {
        res.status(500).send(JSON.stringify('ID buay sai leh'));
    }
};