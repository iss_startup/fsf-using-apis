const config = require('../config'),
      https = require('https'),
      fs = require('fs'),
      rmdir = require('rimraf'),
      FormData = require('form-data'),
      googleMaps = require('@google/maps').createClient(config.gmaps);

var latLngFormat = exports.latLng = function(coordinates) {
    var latLng = [];
    coordinates.forEach(function (coord, index) {
        latLng.push(coord.waypoint.split(','));
    });
    return latLng;
};

exports.findWaypoint = function(req,res){
    return new Promise((resolve, reject) => {
        googleMaps.geocode({
            address: req.place_address
        }, function(err, data) {
            if(err) {
                reject(err);
            }
            if(data.json.results.length == 0) {
                reject(err);
            }
            resolve(data.json.results);
        });
    });
};

exports.genStaticMap = function(coordinates){
    var map = {};
    var markers = [];
    var path = {};
    var route = '';
    return new Promise((resolve, reject) => {
        if(coordinates) {
            map.url = config.gmaps.staticMap + '?size=' + config.gmaps.staticMapSize + '&maptype=' + config.gmaps.staticMapType;
            markers = latLngFormat(coordinates);

            path.waypoints = [];
            markers.forEach(function (marker, index) {
                if(index==0) {
                    path.origin = marker;
                } else {
                    if(index < markers.length-1) {
                        path.waypoints.push(marker);
                    } else {
                        path.destination = marker;
                    }
                }
                map.url = map.url + '&markers=color:red%7C' + marker[0] + ',' + marker[1];
            });

            path.optimize = true;
            path.mode = 'driving';
            console.log(path);

            // Generate polyline
            googleMaps.directions(path, function(err, data) {
                if(err) {
                    reject(err);
                }
                console.log(data.json.routes[0]);

                var polyline = data.json.routes[0].overview_polyline.points;
                // JSON format auto-appends an extra \, resolve this issue by changing it into a URL code
                route = '&path=weight:3%7Ccolor:blue%7Cenc:' + polyline.replace(/\\/g, '%5c');

                map.url = map.url + route + '&format=jpeg&key=' + config.gmaps.clientKey;

                resolve(map);
            });
        } else {
            reject();
        }
    });
};