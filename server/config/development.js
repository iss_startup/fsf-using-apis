
'use strict';

module.exports = {
    debug: true,
    app: {
        host: 'localhost',
        port: 3000
    },
    development: {
        host: 'localhost',
        username: 'fsf_user',
        password: '3z5e4mfHaf2kcu7l',
        database: 'chopeking_db',
        dialect: 'mysql',
        pool:{
            max: 5,
            min: 0,
            idle:10000
        },
        define: {
            paranoid: true,
            underscored: true
        },
        force:false
    },
    storage: {
        credentials: {
            accessKeyId: 'xxxxxxxxxxxxxxxxx',
            secretAccessKey: 'xxxxxxxxxxxxxxxxx',
            region: 'ap-southeast-1'
        },
        domain: 'https://s3-ap-southeast-1.amazonaws.com/',
        bucket: 'chopeah-media',
        tempdir: 'tmp/'
    },
    gmaps: {
        staticMap: 'https://maps.googleapis.com/maps/api/staticmap',
        staticMapSize: '640x360',
        staticMapType: 'roadmap',
        key: 'xxxxxxxxxxxxxxxxx',
        clientKey: 'xxxxxxxxxxxxxxxxx'
    },
    darksky: {
        key: 'xxxxxxxxxxxxxxxxx'
    },
    goodreads: {
        url: 'https://www.goodreads.com',
        key: 'xxxxxxxxxxxxxxxxx',
        secret: 'xxxxxxxxxxxxxxxxx'
    },
    image: {
        thumbnail: {
            width: 200,
            height: 200
        },
        small: {
            width: 640,
            height: 360
        },
        medium: {
            width: 960,
            height: 540
        }
    },
    pagination: {
        default: 50
    },
    mailgun: {
        domain: 'your-domain.com',
        key: 'key-xxxxxxxxxxxxxxxxx',
        sender: 'Jones the Grocer',
        senderEmail: 'jones@thegrocer.com',
        recipient: 'Owner of the Grocer',
        recipientEmail: 'owner@thegrocer.com'
    }
};