const config = require('../config'),
      Sequelize = require('sequelize'),
      JsonType = require('sequelize-json');

module.exports = function (database) {

    var Barangs = database.define('barangs', {
        barang_id: {
            type: Sequelize.BIGINT(20),
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        barang_uuid: {
            type: Sequelize.STRING(50)
        },
        user_uuid: {
            type: Sequelize.STRING(50)
        },
        barang_name: {
            type: Sequelize.STRING(255)
        },
        barang_category: {
            type: Sequelize.STRING(255)
        },
        barang_imgs: JsonType(database, 'Barangs', 'barang_imgs'),
        barang_description: {
            type: Sequelize.TEXT
        },
        barang_status: {
            type: Sequelize.INTEGER(2),
            allowNull: false,
            defaultValue: 0
        }
    }, {
        freezeTableName: true,
        timestamps: true,
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        deletedAt: 'deleted_at'
    });

    return Barangs;

};