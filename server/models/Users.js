var Sequelize = require('sequelize');

module.exports = function (database) {

    var Users = database.define('users', {
        user_id: {
            type: Sequelize.BIGINT(20),
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        user_uuid: {
            type: Sequelize.STRING(50),
            primaryKey: true
        },
        email: {
            type: Sequelize.STRING(255)
        },
        mobile: {
            type: Sequelize.STRING(50)
        },
        password: {
            type: Sequelize.STRING(100)
        },
        remember_token: {
            type: Sequelize.STRING(100)
        },
        consent_token: {
            type: Sequelize.STRING(100)
        },
        role_id: {
            type: Sequelize.BIGINT(20)
        },
        payment_id: {
            type: Sequelize.BIGINT(20)
        }
    }, {
        freezeTableName: true,
        timestamps: true,
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        deletedAt: 'deleted_at'
    });

    Users.associate = function (models) {
        Users.hasOne(models.user_profiles, {foreignKey: 'user_id', targetKey: 'user_id'});
        Users.hasOne(models.user_roles);
        Users.hasMany(models.user_invites);
        Users.hasMany(models.user_password_resets);
    };

    return Users;

};