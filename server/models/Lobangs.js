const config = require('../config'),
    Sequelize = require('sequelize');

module.exports = function (database) {

    var Lobangs = database.define('lobangs', {
        lobang_id: {
            type: Sequelize.BIGINT(20),
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        lobang_uuid: {
            type: Sequelize.STRING(50)
        },
        user_uuid: {
            type: Sequelize.STRING(50)
        },
        lobang_name: {
            type: Sequelize.STRING(255)
        },
        lobang_start: {
            type: Sequelize.DATE
        },
        lobang_end: {
            type: Sequelize.DATE
        },
        lobang_imgs: {
            type: Sequelize.TEXT
        },
        lobang_description: {
            type: Sequelize.TEXT
        },
        lobang_status: {
            type: Sequelize.INTEGER(2),
            allowNull: false,
            defaultValue: 0
        }
    }, {
        getterMethods: {
            lobang_imgs: function () {
                if(typeof this.getDataValue('lobang_imgs')!=='undefined') {
                    var newImages = [];
                    var images = [];
                    images = JSON.parse(this.getDataValue('lobang_imgs'));
                    if(images.length > 0) {
                        images.forEach(function(image, index) {
                            newImages.push(config.storage.domain + config.storage.bucket + '/' + image);
                        });
                    }
                    return newImages;
                } else {
                    return this.getDataValue('lobang_imgs');
                }
            }
        },
        setterMethods: {
            lobang_imgs: function (value) {
                this.setDataValue('lobang_imgs', JSON.stringify(value));
            }
        },
        freezeTableName: true,
        timestamps: true,
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        deletedAt: 'deleted_at'
    });

    return Lobangs;

};