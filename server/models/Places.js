const config = require('../config'),
      Sequelize = require('sequelize');

module.exports = function (database) {

    var Places = database.define('places', {
        place_id: {
            type: Sequelize.BIGINT(20),
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        place_uuid: {
            type: Sequelize.STRING(50)
        },
        user_uuid: {
            type: Sequelize.STRING(50)
        },
        place_name: {
            type: Sequelize.STRING(255)
        },
        place_address: {
            type: Sequelize.STRING(255)
        },
        waypoint: {
            type: Sequelize.TEXT
        },
        place_imgs: {
            type: Sequelize.TEXT
        },
        place_description: {
            type: Sequelize.TEXT
        },
        place_status: {
            type: Sequelize.INTEGER(2),
            allowNull: false,
            defaultValue: 0
        }
    }, {
        getterMethods: {
            place_imgs: function () {
                if(typeof this.getDataValue('place_imgs')!=='undefined') {
                    var newImages = [];
                    var images = [];
                    images = JSON.parse(this.getDataValue('place_imgs'));
                    if(images.length > 0) {
                        images.forEach(function(image, index) {
                            newImages.push(config.storage.domain + config.storage.bucket + '/' + image);
                        });
                    }
                    return newImages;
                } else {
                    return this.getDataValue('place_imgs');
                }
            }
        },
        setterMethods: {
            place_imgs: function (value) {
                this.setDataValue('place_imgs', JSON.stringify(value));
            }
        },
        freezeTableName: true,
        timestamps: true,
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        deletedAt: 'deleted_at'
    });

    return Places;

};