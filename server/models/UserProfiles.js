var Sequelize = require('sequelize');

module.exports = function (database) {

    var UserProfiles = database.define('user_profiles', {
        user_profile_id: {
            type: Sequelize.BIGINT(20),
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        user_id: {
            type: Sequelize.BIGINT(20)
        },
        user_uuid: {
            type: Sequelize.STRING(50)
        },
        name_family: {
            type: Sequelize.STRING(255)
        },
        name_given: {
            type: Sequelize.STRING(255)
        },
        salutation: {
            type: Sequelize.STRING(10)
        },
        gender: {
            type: Sequelize.STRING(10)
        },
        birthdate: {
            type: Sequelize.DATEONLY
        },
        address: {
            type: Sequelize.STRING(255)
        },
        state: {
            type: Sequelize.STRING(255)
        },
        country: {
            type: Sequelize.STRING(255)
        }
    }, {
        freezeTableName: true,
        timestamps: true,
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        deletedAt: 'deleted_at'
    });

    return UserProfiles;

};