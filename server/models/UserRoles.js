var Sequelize = require('sequelize');

module.exports = function (database) {

    var UserRoles = database.define('user_roles', {
        user_role_id: {
            type: Sequelize.BIGINT(20),
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        role: {
            type: Sequelize.STRING(20),
            allowNull: false,
            defaultValue: 'Choper'
        },
        permissions: {
            type: Sequelize.TEXT
        }
    }, {
        freezeTableName: true,
        timestamps: true,
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        deletedAt: 'deleted_at'
    });

    return UserRoles;

};