var Sequelize = require('sequelize');

module.exports = function (database) {

    var UserPasswordResets = database.define('user_password_resets', {
        user_reset_id: {
            type: Sequelize.BIGINT(20),
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        user_uuid: {
            type: Sequelize.STRING(50)
        },
        reset_token: {
            type: Sequelize.STRING(100)
        }
    }, {
        freezeTableName: true,
        timestamps: true,
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        deletedAt: 'deleted_at'
    });

    UserPasswordResets.associate = function (models) {
        UserPasswordResets.belongsTo(models.users);
    };

    return UserPasswordResets;

};