var Sequelize = require('sequelize');

module.exports = function (database) {

    var UserInvites = database.define('user_invites', {
        user_invite_id: {
            type: Sequelize.BIGINT(20),
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        user_uuid: {
            type: Sequelize.STRING(50)
        },
        email: {
            type: Sequelize.STRING(255)
        },
        accepted: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: 0
        }
    }, {
        freezeTableName: true,
        timestamps: true,
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        deletedAt: 'deleted_at'
    });

    UserInvites.associate = function (models) {
        UserInvites.belongsTo(models.users);
    };

    return UserInvites;

};