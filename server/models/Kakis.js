const config = require('../config'),
      Sequelize = require('sequelize');

module.exports = function (database) {

    var Kakis = database.define('kakis', {
        kaki_id: {
            type: Sequelize.BIGINT(20),
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        kaki_uuid: {
            type: Sequelize.STRING(50)
        },
        user_uuid: {
            type: Sequelize.STRING(50)
        },
        kaki_type: {
            type: Sequelize.STRING(255)
        },
        kaki_icon: {
            type: Sequelize.STRING(20)
        }
    }, {
        freezeTableName: true,
        timestamps: true,
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        deletedAt: 'deleted_at'
    });

    Kakis.associate = function (models) {
        Kakis.belongsTo(models.users);
    };

    return Kakis;

};