var Sequelize = require('sequelize');

module.exports = function (database) {

    var Chopes = database.define('chopes', {
        chope_id: {
            type: Sequelize.BIGINT(20),
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        that_uuid: {
            type: Sequelize.STRING(50)
        },
        user_uuid: {
            type: Sequelize.STRING(50)
        },
        that_type: {
            type: Sequelize.STRING(20),
            allowNull: false,
            defaultValue: 'Place'
        }
    }, {
        freezeTableName: true,
        timestamps: true,
        createdAt: 'created_at',
        updatedAt: 'updated_at',
        deletedAt: 'deleted_at'
    });

    Chopes.associate = function (models) {
        Chopes.belongsTo(models.lobangs);
        Chopes.belongsTo(models.places);
        Chopes.belongsTo(models.barangs);
        Chopes.belongsTo(models.users);
    };

    return Chopes;

};