// Copyright Vincent Lau for ISS Learning Day 2016

var googleMapsApiKey = "xxxxxxxxxxxxxxxxxxx";
var forecastApiKey = "xxxxxxxxxxxxxxx";
var giphyApiKey = "xxxxxxxxx";

function showMap(place) {
    var geocoder = new google.maps.Geocoder();

    geocoder.geocode( { 'address': place}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            console.log(results);

            var mapOptions = {
                zoom: 8,
                center: results[0].geometry.location,
                // HYBRID, SATELLITE, ROADMAP, or TERRAIN:
                mapTypeId: google.maps.MapTypeId.HYBRID
            };
            map = new google.maps.Map(document.getElementById("map"), mapOptions);

            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });

            showForecast(
                results[0].geometry.location.lat(), results[0].geometry.location.lng()
            );
        } else {
            alert("Geocode was not successful for the following reason: " + status);
        }
    });
};

function showForecast(lat, lng) {
    $('.js-forecast').empty();

    $.ajax('https://api.forecast.io/forecast/' + forecastApiKey + '/' + lat + ',' + lng, {
        dataType: 'jsonp'
    }).done(function(results) {
        console.log(results);
        for (var n=0; n < results['daily']['data'].length; n++) {
            showDayForecast( results['daily']['data'][n] );
        }
    }).fail(function(jqXHR, textStatus) {
        alert( textStatus );
    });
};

function showDayForecast(forecast) {

    var date = new Date(forecast['time'] * 1000);
    var day = date.getDay(); // 0-6
    var days = [
        'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'
    ];
    var dayOfWeek = days[day];

    var imageUrl = getImage(forecast['icon'], forecast['time']);

    $('.js-forecast').append(
        $('<div class="js-day col-md-3">').html(
                '<h2><span class="day-of-week">' + dayOfWeek + ':</span></h2><p><span class="summary">' + forecast['summary'] + '</span></p>'
            )
            .addClass('js-day-'+forecast['time'] + ' day')
    );
};

function getImage(iconName, time) {
    // Pairing Forecast.io icon values with giphy search strings.
    var searchTerms = {
        "clear-day": "blue sky",
        "clear-night": "stars",
        "rain": "rain",
        "snow": "snow",
        "sleet": "sleet",
        "wind": "tornado",
        "fog": "foggy",
        "cloudy": "clouds",
        "partly-cloudy-day": "cloudy",
        "partly-cloudy-night": "clouds night"
    };

    $.ajax('http://api.giphy.com/v1/gifs/search?q=' + searchTerms[iconName] + '&api_key=' + giphyApiKey
    ).done(function(results) {
        console.log(results);
        var randomImage = randomItem(results['data']);
        var url = randomImage['images']['fixed_width']['url'];
        $('.js-day-'+time).append(
            $('<img class="img-rounded">').prop('src', url)
        );
    }).fail(function(jqXHR, textStatus) {
        alert( textStatus );
    });
};

function randomItem(arrayName) {
    return arrayName[Math.floor(Math.random() * arrayName.length)];
};

$(document).ready(function() {

    $('.js-placeForm').on('submit', function(ev){
        ev.preventDefault();

        if ($('.js-place').prop('value') != '') {
            showMap( $('.js-place').prop('value') );
        };
    });
});